package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        BankAccount obj1 = new BankAccount("Maria","BBVA001");
        obj1.showMenu();
    }
}

class BankAccount {
    int balance;
    int previousTransaction;
    String customerName;
    String customerID;

    public BankAccount(int balance, String customerName, String customerID) {
        this.balance = balance;
        this.customerName = customerName;
        this.customerID = customerID;
    }

    public BankAccount(String customerName, String customerID) {
        this.customerName = customerName;
        this.customerID = customerID;
        this.balance = 0;
    }

    void deposit (int amount){
        if (amount !=0) {
            balance += amount;
            previousTransaction = amount;
            System.out.println("Successfully deposited " + amount + "€");
        }
        else System.out.println("Action was unsuccessful, please make sure the amount you are trying to deposit is greater than 0");
    }

    void withdraw (int amount) {
        if (amount <= balance) {
            balance -= amount;
            previousTransaction = -amount;
            System.out.println("Successfully withdrew " + amount + "€");
        }
        else System.out.println("Action was unsuccessful, please make sure the amount you are trying to withdraw is not greater than your available balance");

    }

    void getPreviousTransaction (){
        if(previousTransaction > 0) {
            System.out.println("Deposited : " + previousTransaction);
        }
        else if (previousTransaction < 0) {
            System.out.println("Withdrew : " + Math.abs(previousTransaction));
        }
        else System.out.println("No previous transaction found");
    }

    void showMenu() {
        char option;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Welcome customer " + customerName + " ID : " + customerID);
        System.out.println("\n");
        System.out.println("A : Check balance");
        System.out.println("B : Deposit");
        System.out.println("C : Withdraw");
        System.out.println("D : Previous transaction");
        System.out.println("E : Exit");

        do {
            System.out.println("*************************************************************");
            System.out.println("Enter an option");
            System.out.println("*************************************************************");
            option = scanner.next().toUpperCase().charAt(0);
            System.out.println("\n");

            switch (option){
                case 'A' :
                    System.out.println("------------------------------------------------");
                    System.out.println("Your current balance is : " + balance);
                    System.out.println("------------------------------------------------");
                    System.out.println("\n");
                    break;

                case 'B' :
                    System.out.println("------------------------------------------------");
                    System.out.println("Enter an amount to deposit");
                    int amount = scanner.nextInt();
                    deposit(amount);
                    System.out.println("\n");
                    break;

                case 'C' :
                    System.out.println("------------------------------------------------");
                    System.out.println("Enter an amount to withdraw");
                    amount = scanner.nextInt();
                    withdraw(amount);
                    System.out.println("\n");
                    break;

                case 'D' :
                    System.out.println("------------------------------------------------");
                    getPreviousTransaction();
                    System.out.println("------------------------------------------------");
                    System.out.println('\n');
                    break;

                case 'E' :
                    System.out.println("*************************************************************");
                    break;

                default:
                    System.out.println("Invalid option, please refer to the menu and pick a valid option.");
                    break;
            }
        }
        while (option != 'E');

        System.out.println("Thank you for using our services!");
    }
}